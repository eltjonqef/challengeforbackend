var express = require("express");
var app = express();
const User = require('./user.js');
const mongoose = require('mongoose');
const swaggerValidation = require('openapi-validator-middleware');
swaggerValidation.init('./challengeForBackend.yaml');
const jwt= require('jsonwebtoken');
const objectID = mongoose.Types.ObjectId;
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

mongoose.connect('mongodb://localhost:27017/test', {useNewUrlParser: true, useUnifiedTopology: true});
const db=mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));

db.once('open', function(){
    app.get('/users', swaggerValidation.validate, authenticate, function (req, res){
        const query=User.find();
        query.exec(function (err, users){
            res.setHeader("Content-Type", "application/json");
            res.end(JSON.stringify(users));
        });
    });

    app.get('/users/:id', swaggerValidation.validate, authenticate, function (req, res){
        const objectId= new objectID(req.params.id);
        const query=User.findOne({'_id' : objectId});
        query.exec(function (err, user){
            res.setHeader("Content-Type", "application/json");
            res.end(JSON.stringify(user));
        });
    });

    app.post('/users', swaggerValidation.validate, authenticate, function (req, res) {
        let user = new User({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            password: req.body.password
        });
        user.save(function (err){
            if(err) return handleError(err);
            res.setHeader("Content-Type", "application/json");
            res.end();
        });
    });

    app.put('/users/:id', swaggerValidation.validate, authenticate, function(req, res){
        const objectId= new objectID(req.params.id);
        const query=User.updateOne({'_id' : objectId}, {'email': req.body.email, 'password': req.body.password});
        query.exec(function (err, user){
            res.setHeader("Content-Type", "application/json");
            res.end(JSON.stringify(user));
        });
    });
    
    app.delete('/users:id', swaggerValidation.validate, authenticate, function (req, res){
        const objectId= new objectID(req.params.id);
        const query=User.deleteOne({'_id' : objectId});
        query.exec(function (err){
            res.setHeader("Content-Type", "application/json");
            res.end();
        });
    });

    app.post('/login', swaggerValidation.validate, function(req, res){
        const userLogin={
            email: req.body.email,
            password: req.body.password
        }
        jwt.sign({user: userLogin}, 'secret?', {expiresIn: '5m'}, function(err, token){
            if(err) res.status(400).end();
            res.setHeader("Content-Type", "application/json");
            res.end(JSON.stringify({"token":token}));
        });
    });

    app.use((err, req, res, next) => {
        if (err instanceof swaggerValidation.InputValidationError) {
            return res.status(400).json({ more_info: JSON.stringify(err.errors) });
        }
    });
});


var server = app.listen(8081, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log("Example app listening at http://%s:%s", host, port);
})

function authenticate(req, res, next){
    const authHeader=req.headers['authorization'];
    const token=authHeader.split(' ')[1];
    if(token==null)
        return res.status(401).end(JSON.stringify("Unauthorized!"));
    jwt.verify(token, 'secret?', function(err, user){
        if(err)
            return res.status(401).end(JSON.stringify("Unauthorized!"));
        req.user=user;
        next();
    });
}